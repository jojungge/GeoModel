
# Set up the version of GeoModel as a cache variable, so that other
# sub-projects could use this value.
set( GeoModel_VERSION "4.1.1.1" CACHE STRING
    "Version of the GeoModel project" )

